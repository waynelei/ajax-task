class Task < ActiveRecord::Base
	validates :description,  presence: true, length: { maximum: 50 }
	validates :deadline,  presence: true
end
