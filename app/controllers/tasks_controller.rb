class TasksController < ApplicationController
  before_action :all_tasks, only: [:index, :create, :update, :destroy]
  before_action :set_tasks, only: [:edit, :update, :destroy]


  def new
    @task = Task.new
  end

  def create
    #@task = Task.create(task_params)

    @task = Task.new(task_params)
    if @task.save
      flash[:success] = "Task Create"
    else
      render 'new'
    end

  end



  def update
    #puts task_params
    #@task.update_attributes(task_params)

    if @task.update_attributes(task_params)
      flash[:success] = "Task updated"
    else
      render 'edit'
    end

  end




  def destroy
    @d_id = @task.id
    p "d_id is #{@d_id} ======================"
    @task.destroy
  end

  private

    def all_tasks
      @tasks = Task.all
    end

    def set_tasks
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:description, :deadline)
    end
end
